# README #

Welcome to the levAR Social Media Sandboxes

### What is this repository for? ###

* Sandboxing AR filters for social media platforms

### How do I get set up? ###

* Get a Facebook
* Get a Snapchat
* (Recommended) Get an Instagram
* Install Spark AR (MacOS, Windows)
* Install Lens Studio (MacOS, Windows)

### Contribution guidelines ###

* Create a folder inside /spark or /lensStudio for each sandbox

### Who do I talk to? ###

* Vik Bhaduri
* Ben Chapman