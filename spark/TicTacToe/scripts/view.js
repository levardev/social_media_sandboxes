import Scene from "Scene";
import Materials from "Materials";

import {
    Diagnostics,
    initStart
} from './controller';
import {
    get
} from "./model"

(async function() {

    let verboseMode = get("verboseMode");
    verboseMode && Diagnostics.log("VIEW | initialized");

    //Create references to scene objects
    const [focalDistance, e, s, r, playerOne, playerTwo] = await Promise.all([
        Scene.root.findFirst('Focal Distance'),
        Materials.findFirst('mat_UI_empty'),
        Materials.findFirst('mat_UI_start'),
        Materials.findFirst("mat_UI_replay"),
        Scene.root.findFirst('playerOne'),
        Scene.root.findFirst('playerTwo')
    ]);

    //Set Player text to the non-score value
    playerOne.text = "X";
    playerTwo.text = "O";

    verboseMode && Diagnostics.log("VIEW | Player text set");

    //BEGIN Create board Planes
    const plane0 = await Scene.create("Plane", {
        "name": "plane0",
        "width": 0.05,
        "height": 0.05,
        "x": -0.075,
        "y": 0.05,
        "hidden": true,
        "material": e
    });

    verboseMode && Diagnostics.log("VIEW | plane0 created");

    const plane1 = await Scene.create("Plane", {
        "name": "plane1",
        "width": 0.05,
        "height": 0.05,
        "x": 0,
        "y": 0.05,
        "hidden": true,
        "material": e
    });

    verboseMode && Diagnostics.log("VIEW | plane1 created");

    const plane2 = await Scene.create("Plane", {
        "name": "plane2",
        "width": 0.05,
        "height": 0.05,
        "x": 0.075,
        "y": 0.05,
        "hidden": true,
        "material": e
    });

    verboseMode && Diagnostics.log("VIEW | plane2 created");

    const plane3 = await Scene.create("Plane", {
        "name": "plane3",
        "width": 0.05,
        "height": 0.05,
        "x": -0.075,
        "y": -0.025,
        "hidden": true,
        "material": e
    });

    verboseMode && Diagnostics.log("VIEW | plane3 created");

    const plane4 = await Scene.create("Plane", {
        "name": "plane4",
        "width": 0.05,
        "height": 0.05,
        "x": 0,
        "y": -0.025,
        "hidden": true,
        "material": e
    });

    verboseMode && Diagnostics.log("VIEW | plane4 created");

    const plane5 = await Scene.create("Plane", {
        "name": "plane5",
        "width": 0.05,
        "height": 0.05,
        "x": 0.075,
        "y": -0.025,
        "hidden": true,
        "material": e
    });

    verboseMode && Diagnostics.log("VIEW | plane5 created");

    const plane6 = await Scene.create("Plane", {
        "name": "plane6",
        "width": 0.05,
        "height": 0.05,
        "x": -0.075,
        "y": -0.1,
        "hidden": true,
        "material": e
    });

    verboseMode && Diagnostics.log("VIEW | plane6 created");

    const plane7 = await Scene.create("Plane", {
        "name": "plane7",
        "width": 0.05,
        "height": 0.05,
        "x": 0,
        "y": -0.1,
        "hidden": true,
        "material": e
    });

    verboseMode && Diagnostics.log("VIEW | plane7 created");

    const plane8 = await Scene.create("Plane", {
        "name": "plane8",
        "width": 0.05,
        "height": 0.05,
        "x": 0.075,
        "y": -0.1,
        "hidden": true,
        "material": e
    });

    verboseMode && Diagnostics.log("VIEW | plane8 created");
    //END Create board Planes

    //Create start button Plane
    const planeStart = await Scene.create("Plane", {
        "name": "planeStart",
        "width": 0.12,
        "height": 0.12,
        "x": 0,
        "y": -0.075,
        "hidden": false,
        "material": s
    });

    verboseMode && Diagnostics.log("VIEW | planeStart created");

    //Create play again button Plane
    const planeAgain = await Scene.create("Plane", {
        "name": "planeAgain",
        "width": 0.09,
        "height": 0.09,
        "x": 0,
        "y": -0.11,
        "hidden": true,
        "material": r
    });

    verboseMode && Diagnostics.log("VIEW | planeAgain created");

    //BEGIN Add UI elements to scene
    focalDistance.addChild(plane0);
    focalDistance.addChild(plane1);
    focalDistance.addChild(plane2);
    focalDistance.addChild(plane3);
    focalDistance.addChild(plane4);
    focalDistance.addChild(plane5);
    focalDistance.addChild(plane6);
    focalDistance.addChild(plane7);
    focalDistance.addChild(plane8);
    focalDistance.addChild(planeStart);
    focalDistance.addChild(planeAgain);

    verboseMode && Diagnostics.log("VIEW | UI elements added to focalDistance");
    //END Add UI elements to focalDistance

    //Call initStart from Controller.js beginning the game logic
    initStart();

})();