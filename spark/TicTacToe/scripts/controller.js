import Scene from "Scene";
import Materials from "Materials";
import {
    get,
    set
} from "./model"
import TouchGestures from "TouchGestures";

const Diagnostics = require('Diagnostics');

let verboseMode = get("verboseMode");

const initStart = () => {

    verboseMode && Diagnostics.log("CONTROLLER | initStart initialized");
    
    (async function() {

        const [planeStart] = await Promise.all([
            Scene.root.findFirst('planeStart'),
        ]);

        TouchGestures.onTap(planeStart).subscribe((gesture) => {
            initBoard();
        });

        return null;
    })();

    return null;

}

const initBoard = () => {

    verboseMode && Diagnostics.log("CONTROLLER | initBoard initialized");

    set("turn", 1);

    (async function() {

        const [planeStart, plane0, plane1, plane2, plane3, plane4, plane5, plane6, plane7, plane8] = await Promise.all([
            Scene.root.findFirst('planeStart'),
            Scene.root.findFirst('plane0'),
            Scene.root.findFirst('plane1'),
            Scene.root.findFirst('plane2'),
            Scene.root.findFirst('plane3'),
            Scene.root.findFirst('plane4'),
            Scene.root.findFirst('plane5'),
            Scene.root.findFirst('plane6'),
            Scene.root.findFirst('plane7'),
            Scene.root.findFirst('plane8')
        ]);

        planeStart.hidden = true;
        plane0.hidden = false;
        plane1.hidden = false;
        plane2.hidden = false;
        plane3.hidden = false;
        plane4.hidden = false;
        plane5.hidden = false;
        plane6.hidden = false;
        plane7.hidden = false;
        plane8.hidden = false;

        TouchGestures.onTap(plane0).subscribe((gesture) => {
            attemptTurn(0);
        });

        TouchGestures.onTap(plane1).subscribe((gesture) => {
            attemptTurn(1);
        });

        TouchGestures.onTap(plane2).subscribe((gesture) => {
            attemptTurn(2);
        });

        TouchGestures.onTap(plane3).subscribe((gesture) => {
            attemptTurn(3);
        });

        TouchGestures.onTap(plane4).subscribe((gesture) => {
            attemptTurn(4);
        });

        TouchGestures.onTap(plane5).subscribe((gesture) => {
            attemptTurn(5);
        });

        TouchGestures.onTap(plane6).subscribe((gesture) => {
            attemptTurn(6);
        });

        TouchGestures.onTap(plane7).subscribe((gesture) => {
            attemptTurn(7);
        });

        TouchGestures.onTap(plane8).subscribe((gesture) => {
            attemptTurn(8);
        });

        return null;

    })();

    return null;

};

const attemptTurn = (boardIndex) => {

    verboseMode && Diagnostics.log("CONTROLLER | attemptTurn initialized | " + boardIndex);

    const boardArray = get("boardArray");

    if (get("turn") === 1) {

        if (boardArray[boardIndex] === 0) {
            setBoardMaterial(boardIndex, "x");
            set("boardArrayIndex", boardIndex, 1);
            checkWin(1);
        }

    } else {

        if (boardArray[boardIndex] === 0) {
            setBoardMaterial(boardIndex, "o");
            set("boardArrayIndex", boardIndex, 2);
            checkWin(2);
        }

    };

    return null;

};

const setBoardMaterial = (boardIndex, material) => {

    verboseMode && Diagnostics.log("CONTROLLER | setBoardMaterial initialized | " + boardIndex + " | " + material);

    (async function() {

        let inputPlane = "";
        switch (boardIndex) {
            case 0:
                inputPlane = "plane0";
                break;
            case 1:
                inputPlane = "plane1";
                break;
            case 2:
                inputPlane = "plane2";
                break;
            case 3:
                inputPlane = "plane3";
                break;
            case 4:
                inputPlane = "plane4";
                break;
            case 5:
                inputPlane = "plane5";
                break;
            case 6:
                inputPlane = "plane6";
                break;
            case 7:
                inputPlane = "plane7";
                break;
            case 8:
                inputPlane = "plane8";
                break;
            default:
                break;
        }

        const [plane, x, o, e] = await Promise.all([
            Scene.root.findFirst(inputPlane),
            Materials.findFirst('mat_UI_x'),
            Materials.findFirst('mat_UI_o'),
            Materials.findFirst('mat_UI_empty')
        ]);

        if (material === "x") {
            plane.material = x;
        }

        if (material === "o") {
            plane.material = o;
        }

        if (material === "e") {
            plane.material = e;
        }

        return null;

    })();
};

const checkWin = (turn) => {

    verboseMode && Diagnostics.log("CONTROLLER | checkWin initialized");

    const boardArray = get("boardArray");

    if (turn === 1) {
        if (
            (boardArray[0] === boardArray[3] && boardArray[3] === boardArray[6] && boardArray[6] === 1) ||
            (boardArray[0] === boardArray[1] && boardArray[1] === boardArray[2] && boardArray[2] === 1) ||
            (boardArray[2] === boardArray[5] && boardArray[5] === boardArray[8] && boardArray[8] === 1) ||
            (boardArray[6] === boardArray[7] && boardArray[7] === boardArray[8] && boardArray[8] === 1) ||
            (boardArray[3] === boardArray[4] && boardArray[4] === boardArray[5] && boardArray[5] === 1) ||
            (boardArray[1] === boardArray[4] && boardArray[4] === boardArray[7] && boardArray[7] === 1) ||
            (boardArray[0] === boardArray[4] && boardArray[4] === boardArray[8] && boardArray[8] === 1) ||
            (boardArray[6] === boardArray[4] && boardArray[4] === boardArray[2] && boardArray[2] === 1)
        ) {
            setWin(1);
            return null;
        };
    } else if (turn === 2) {
        if (
            (boardArray[0] === boardArray[3] && boardArray[3] === boardArray[6] && boardArray[6] === 2) ||
            (boardArray[0] === boardArray[1] && boardArray[1] === boardArray[2] && boardArray[2] === 2) ||
            (boardArray[2] === boardArray[5] && boardArray[5] === boardArray[8] && boardArray[8] === 2) ||
            (boardArray[6] === boardArray[7] && boardArray[7] === boardArray[8] && boardArray[8] === 2) ||
            (boardArray[3] === boardArray[4] && boardArray[4] === boardArray[5] && boardArray[5] === 2) ||
            (boardArray[1] === boardArray[4] && boardArray[4] === boardArray[7] && boardArray[7] === 2) ||
            (boardArray[0] === boardArray[4] && boardArray[4] === boardArray[8] && boardArray[8] === 2) ||
            (boardArray[6] === boardArray[4] && boardArray[4] === boardArray[2] && boardArray[2] === 2)
        ) {
            setWin(2);
            return null;
        };
    }

    if (boardArray[0] != 0 && boardArray[1] != 0 && boardArray[2] != 0 && boardArray[3] != 0 && boardArray[4] != 0 && boardArray[5] != 0 && boardArray[6] != 0 && boardArray[7] != 0 && boardArray[8] != 0) {
        clearBoard();
        return null;
    };

    if (turn === 1) {
        set("turn", 2);
    } else {
        set("turn", 1);
    };

    return null;

};

const setWin = (player) => {

    verboseMode && Diagnostics.log("CONTROLLER | setWin initialized | " + player);

    (async function() {

        const [plane0, plane1, plane2, plane3, plane4, plane5, plane6, plane7, plane8, planeAgain, playerOne, playerTwo] = await Promise.all([
            Scene.root.findFirst('plane0'),
            Scene.root.findFirst('plane1'),
            Scene.root.findFirst('plane2'),
            Scene.root.findFirst('plane3'),
            Scene.root.findFirst('plane4'),
            Scene.root.findFirst('plane5'),
            Scene.root.findFirst('plane6'),
            Scene.root.findFirst('plane7'),
            Scene.root.findFirst('plane8'),
            Scene.root.findFirst('planeAgain'),
            Scene.root.findFirst('playerOne'),
            Scene.root.findFirst('playerTwo')
        ]);

        plane0.hidden = true;
        plane1.hidden = true;
        plane2.hidden = true;
        plane3.hidden = true;
        plane4.hidden = true;
        plane5.hidden = true;
        plane6.hidden = true;
        plane7.hidden = true;
        plane8.hidden = true;
        planeAgain.hidden = false;

        TouchGestures.onTap(planeAgain).subscribe((gesture) => {
            clearBoard();
        });

        let score = get("score");

        if (player === 1) {
            set("turn", 3);
            score[0] += 1;
            playerOne.text = String(score[0]);
            playerTwo.text = String(score[1]);
        } else if (player === 2) {
            set("turn", 4);
            score[1] += 1;
            playerOne.text = String(score[0]);
            playerTwo.text = String(score[1]);
        };

        set("score", score);

        return null;

    })();

    return null;

};

const clearBoard = () => {

    verboseMode && Diagnostics.log("CONTROLLER | clearBoard initialized");

    (async function() {

        const [plane0, plane1, plane2, plane3, plane4, plane5, plane6, plane7, plane8, planeAgain, playerOne, playerTwo] = await Promise.all([
            Scene.root.findFirst('plane0'),
            Scene.root.findFirst('plane1'),
            Scene.root.findFirst('plane2'),
            Scene.root.findFirst('plane3'),
            Scene.root.findFirst('plane4'),
            Scene.root.findFirst('plane5'),
            Scene.root.findFirst('plane6'),
            Scene.root.findFirst('plane7'),
            Scene.root.findFirst('plane8'),
            Scene.root.findFirst('planeAgain'),
            Scene.root.findFirst('playerOne'),
            Scene.root.findFirst('playerTwo')
        ]);

        plane0.hidden = false;
        plane1.hidden = false;
        plane2.hidden = false;
        plane3.hidden = false;
        plane4.hidden = false;
        plane5.hidden = false;
        plane6.hidden = false;
        plane7.hidden = false;
        plane8.hidden = false;
        planeAgain.hidden = true;
        playerOne.text = "X";
        playerTwo.text = "O"

        setBoardMaterial(0, "e");
        setBoardMaterial(1, "e");
        setBoardMaterial(2, "e");
        setBoardMaterial(3, "e");
        setBoardMaterial(4, "e");
        setBoardMaterial(5, "e");
        setBoardMaterial(6, "e");
        setBoardMaterial(7, "e");
        setBoardMaterial(8, "e");
        set("boardArray", [0, 0, 0, 0, 0, 0, 0, 0, 0]);
        set("turn", 1);

        return null;

    })();

    return null;

};

export {
    initStart,
    Diagnostics
};