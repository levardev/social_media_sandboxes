const Diagnostics = require('Diagnostics');

//VERBOSE MODE | Set this to true to enable robust console logging. Set to false to focus on newly added debug console logs. 
let verboseMode = false;

//TURN | 0 = Pregame, 1 = Player One's Turn, 2 = Player Two's Turn, 3 = Player One Wins, 4 = Player Two Wins 
let turn = 0;

//BOARD ARRAY | The indeces of this array represent the playspace, like a phone dialpad 0-9. Index values: 0 = Empty, 1 = X, 2 = O
let boardArray = [0, 0, 0, 0, 0, 0, 0, 0, 0];

//SCORE | The score of the Players. [PlayerOne, PlayerTwo]
let score = [0, 0];

const get = (parameter) => {
    switch (parameter) {
        case "verboseMode":
            verboseMode && Diagnostics.log("MODEL | get | verboseMode | " + verboseMode);
            return verboseMode;
        case "turn":
            verboseMode && Diagnostics.log("MODEL | get | turn | " + turn);
            return turn;
        case "boardArray":
            verboseMode && Diagnostics.log("MODEL | get | boardArray | " + boardArray);
            return boardArray;
        case "score":
            verboseMode && Diagnostics.log("MODEL | get | score | " + score);
            return score;
        default:
            return null;
    };
};

const set = (parameter, value1, value2) => {
    switch (parameter) {
        case "verboseMode":
            verboseMode && Diagnostics.log("MODEL | set | verboseMode | " + value1);
            verboseMode = value1;
            break;
        case "turn":
            verboseMode && Diagnostics.log("MODEL | set | turn | " + value1);
            turn = value1;
            break;
        case "boardArray":
            verboseMode && Diagnostics.log("MODEL | set | boardArray | " + value1);
            boardArray = value1;
            break;
        case "boardArrayIndex":
            verboseMode && Diagnostics.log("MODEL | set | boardArrayIndex | " + value1 + " | " + value2);
            boardArray[value1] = value2;
            break;
        case "score":
            verboseMode && Diagnostics.log("MODEL | set | score | " + value1);
            score = value1;
            break;
        default:
            break;
    };
    return null;
};


export {
    get,
    set
};