import {get, set} from "./model";
const console = require('Diagnostics');
const verboseMode = get("verboseMode");

const UIClearAllPressed = () => {
  verboseMode && console.log("CONTROLLER | UIClearAllPressed");
  ClearAll();
  return null;
};

const UIClearEntryPressed = () => {
  verboseMode && console.log("CONTROLLER | UIClearEntryPressed");
  ClearEntry();
  return null;
};

const UIMemoryRecall = () => {
  verboseMode && console.log("CONTROLLER | UIMemoryRecall");
  let memory = get("memory");
  if (!memory){
    console.log("fuck yeah")
    set("memory", get("entry"));
    ClearEntry();
  } else {
    ReplaceEntry(memory);
  };
  return null;
};

const UIIntegerPressed = (parameter) => {
  verboseMode && console.log("CONTROLLER | UIIntegerPressed | " + parameter);
  const potentialOperation = get("potentialOperation");
  if (!potentialOperation){
    ModifyEntry(parameter);
  } else {
    set("definiteOperation", potentialOperation);
    set("calculation", get("entry"));
    ReplaceEntry(parameter);
  }
  return null;
};
  
const UIBasicOperatorPressed = (parameter) => {
  verboseMode && console.log("CONTROLLER | UIBasicOperatorPressed | " + parameter);
  get("entry") && get("calculation") && UIEqualsPressed();
  switch(parameter){
    case "+":
      set("potentialOperation", "+");
      break;
    case "-":
      set("potentialOperation", "-");
      break;
    case "x":
      set("potentialOperation", "x");
      break;
    case "/":
      set("potentialOperation", "/");
      break;
    default:
      break;
  }
  return null
};

const UIEqualsPressed = () => {
  verboseMode && console.log("CONTROLLER | UIEqualsPressed");
  const entry = get("entry");
  const calculation = get("calculation");
  const definiteOperation = get("definiteOperation") 
  let newEntry = 0;
  switch(definiteOperation){
    case "+":
      newEntry = calculation + entry;
      break;
    case "-":
      newEntry = calculation - entry;
      break;
    case "x":
      newEntry = calculation * entry;
      break;
    case "/":
      newEntry = calculation / entry;
      break;
    default:
  }; 
  ReplaceEntry(newEntry);
  set("calculation", null);
  set("potentialOperation", null);
  set("definiteOperation", null)
  return null;
};

const ReplaceEntry = (parameter) => {
  verboseMode && console.log("CONTROLLER | ReplaceEntry | " + parameter);
  set("entry", parameter);
  return null;
}

const ModifyEntry = (parameter) => {
  verboseMode && console.log("CONTROLLER | ModifyEntry | " + parameter);
  const entryArray = [get("entry")];
  entryArray.push(parameter);
  set("entry", Number(entryArray.join("")));
  return null;
}

const ClearEntry = () => {
  verboseMode && console.log("CONTROLLER | ClearEntry");
  set("entry", 0);
  return null;
}

const ClearAll = () => {
  verboseMode && console.log("CONTROLLER | ClearAll");
  set("calculation", null);
  ClearEntry();
  return null;
}

(async function () {  // Enables async/await in JS [part 1]
  UIIntegerPressed(2);
  // UIIntegerPressed(2);
  // UIClearEntryPressed();
  UIBasicOperatorPressed("+");
  UIIntegerPressed(1);
  // UIBasicOperatorPressed("+");
  // UIIntegerPressed(1);
  // UIClearEntryPressed();
  // UIIntegerPressed(4);
  // 
  // UIIntegerPressed(3);
  // UIBasicOperatorPressed("+");
  UIBasicOperatorPressed("-");
  UIIntegerPressed(2);
  UIEqualsPressed();
})(); // Enables async/await in JS [part 2]