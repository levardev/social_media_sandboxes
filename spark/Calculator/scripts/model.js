const console = require('Diagnostics');

//VERBOSE MODE | Set this to true to enable robust console logging. Set to false to focus on newly added debug console logs. 
let verboseMode = true;

let calculation = 0;
let entry = 0;
let memory = null;
let potentialOperation = null;
let definiteOperation = null;
let negateEntry = false;

const get = (parameter) => {
  switch (parameter) {
    case "verboseMode":
      verboseMode && console.log("MODEL | get | verboseMode | " + verboseMode);
      return verboseMode;
    case "calculation":
      verboseMode && console.log("MODEL | get | calculation | " + calculation);
      return calculation;
    case "entry":
      verboseMode && console.log("MODEL | get | entry | " + entry);
      return entry;
    case "memory":
      verboseMode && console.log("MODEL | get | memory | " + memory);
      return memory;
    case "potentialOperation":
      verboseMode && console.log("MODEL | get | potentialOperation | " + potentialOperation);
      return potentialOperation;
    case "definiteOperation":
      verboseMode && console.log("MODEL | get | definiteOperation | " + definiteOperation);
      return definiteOperation;
    case "negateEntry":
      verboseMode && console.log("MODEL | get | negateEntry | " + negateEntry);
      return negateEntry;
    default:
      return null;
  };
};

const set = (parameter, value1) => {
  switch (parameter) {
    case "verboseMode":
      verboseMode && console.log("MODEL | set | verboseMode | " + value1);
      verboseMode = value1;
      break;
    case "calculation":
      verboseMode && console.log("MODEL | set | calculation | " + value1);
      calculation = value1;
      break;
    case "entry":
      verboseMode && console.log("MODEL | set | entry | " + value1);
      entry = value1;
      break;
    case "memory":
      verboseMode && console.log("MODEL | set | memory | " + value1);
      memory = value1;
      break;
    case "potentialOperation":
      verboseMode && console.log("MODEL | set | potentialOperation | " + value1);
      potentialOperation = value1;
      break;
    case "definiteOperation":
      verboseMode && console.log("MODEL | set | definiteOperation | " + value1);
      definiteOperation = value1;
      break;
    case "negateEntry":
      verboseMode && console.log("MODEL | set | negateEntry | " + value1);
      negateEntry = value1;
      break;
    default:
      break;
    };
  return null;
};

export {get, set};

/*
  CONCEPT/VERB_____Abrv.__dev___pair________
  
  clearEntry       CE     alpha
  clearAll         AC     alpha

  invert           inv    pre

  start            ON     pre   OFF
  stop             OFF    pre   ON     

  memoryRecall     mr     pre   randomRecall
  randomRecall     rand   pre   memoryRecall
  memoryClear      mc     pre

  m                m      pre   in
  in               in     pre   cm   

  scientificPower  EXP    pre

  radians          rad    pre   degrees
  degrees          deg    pre   radians

  ln               ln     pre   ex
  ex               ex     pre   ln

  log              log    pre   10x
  10x              10x    pre   log

  e                e      pre

  sin              sin    pre   invSin
  invSin           sin-1  pre   sin

  cos              cos    pre   invCos
  invCose          cos-1  pre   cos

  tan              tan    pre   invTan
  invTan           tan-1  pre   tan

  pi               π      pre   

  square           x2     pre   squareRoot
  sqaureRoot       √x     pre   square

  xy               xy     pre   y√x  
  y√x              y√x    pre   xy

  percent          %      pre    

  divide           ÷      alpha

  seven            7      alpha
  eight            8      alpha
  nine             9      alpha

  mulitply         x      alpha

  four             4      alpha
  five             5      alpha
  six              6      alpha
  
  subtract         -      alpha

  one              1      alpha
  two              2      alpha
  three            3      alpha

  add              +      alpha
  
  zero             0      alpha

  decimal          .      pre
 
  negate           +/-    pre
 
  equals           =      alpha


__________________________
|||||||||||||          |~~|
|||||||||||||__________|~~|
|`````````````````````````|
|_________________________|
|  __    __    ___   __   |
| |AC|  |CE|  |inv| |ON|  |
|  __    __    __    ___  |
| |mr|  |mc|  |cm|  |EXP| |
|  ___   __    ___   _    |
| |rad| |ln|  |log| |e|   |
|  ___   ___   ___   _    |
| |sin| |cos| |tan| |π|   |
|  __    __    _     _    |
| |x2|  |xy|  |%|   |÷|   |
|  _     _     _     _    |
| |7|   |8|   |9|   |x|   |
|  _     _     _     _    |
| |4|   |5|   |6|   |-|   | 
|  _     _     _     _    |  
| |1|   |2|   |3|   |+|   |
|  _     _     ___   _    |
| |0|   |.|   |+/-| |=|   |
|_________________________|

*/